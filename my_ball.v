`timescale 1ns / 1ps

module my_ball(
input clk,reset,
input wire [10:0] x,y,
input wire [10:0] ball_start_x,ball_start_y,ir_x,ir_y,
output reg [2:0] rgb,
input [2:0] ball_rgb
    );
	 
wire tick;
assign tick = ((x == 0) && (y == 601));

localparam WIDTH = 800;
localparam HEIGHT = 600;

localparam BALL_SIZE = 20;

wire ball_on;

wire [10:0] ball_x_l, ball_x_r, ball_y_t, ball_y_b, ball_x_next, ball_y_next;
reg [10:0] ball_x_reg, ball_y_reg;

assign ball_x_l =  ball_x_reg;
assign ball_x_r =  ball_x_reg + BALL_SIZE - 1;
assign ball_y_t =  ball_y_reg;
assign ball_y_b =  ball_y_reg + BALL_SIZE - 1;


reg [10:0] x_d_r, x_d_n, y_d_r, y_d_n; 

assign ball_on = (ball_x_l < x ) && (ball_y_t < y ) && (x < ball_x_r) && (y < ball_y_b);

always@(posedge clk, posedge reset)
	if(reset)
		begin
			ball_x_reg 	<= ball_start_x;
			ball_y_reg 	<= ball_start_y;
			x_d_r 		<=	ir_x;
			y_d_r 		<=	ir_y;
		end
	else
		begin
			ball_x_reg 	<= ball_x_next;
			ball_y_reg 	<= ball_y_next;
			x_d_r 		<=	x_d_n;
			y_d_r 		<=	y_d_n;
		end
			
assign ball_x_next = (tick) ? ball_x_reg + x_d_r : ball_x_reg;	
assign ball_y_next = (tick) ? ball_y_reg + y_d_r : ball_y_reg;
			
always@*
	begin
		x_d_n = x_d_r;
		y_d_n = y_d_r;
		
		if( ball_y_t < 2 )
			 y_d_n = 2;
		else if( ball_y_b > ( HEIGHT - 1 ) )
				y_d_n = -2;
				else if( ball_x_l < 3 )
							x_d_n = 2;
							else if( ball_x_r > ( WIDTH - 3) )
								x_d_n = -2;
	end
							
always @*
	if(ball_on)
		rgb = ball_rgb;
	else
		rgb = 3'b000;

endmodule
 