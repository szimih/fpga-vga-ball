`timescale 1ns / 1ps
module top_module(
input  wire clk, reset, 
input  wire [2:0] sw, 
output wire hsync , vsync, 
output wire [2:0] rgb
    );
	 
wire  [2:0] rgb_reg[7:0]; 
wire video_on; 
wire clk_50;
wire [10:0] x,y;
genvar i;

clock clk_s(.CLK_IN1(clk),.CLK_OUT1(clk_50),.RESET(~reset)); 

vga_sync_my vga(.clk(clk_50), .reset(~reset), .h_sync(hsync) , .v_sync(vsync), .video_on(video_on), .pos_x(x), .pos_y(y));

generate
	for(i=1;i<9;i=i+1)
		begin: val
			my_ball ball_i(.clk(clk_50),.reset(~reset),.x(x),.y(y),.rgb(rgb_reg[i-1]),.ball_rgb(i),.ball_start_x(i*i*i*(-19)),.ball_start_y(i*i*i*(-33)),.ir_x(i*i*i),.ir_y(i*i*i*(-1)));
end
endgenerate 

assign rgb = (video_on) ?  (rgb_reg[0] | rgb_reg[1] | rgb_reg[2] | rgb_reg[3] | rgb_reg[4] | rgb_reg[5] | rgb_reg[6] | rgb_reg[7]) :  3'b000; 

endmodule
